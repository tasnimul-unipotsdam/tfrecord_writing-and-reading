import os
import matplotlib.pyplot as plt

import tensorflow as tf

from reading_record import DataLoader


os.environ['TF_CPP_MIN_LOG_LEVEL'] = '1'
devises = tf.config.experimental.list_physical_devices('GPU')
tf.config.experimental.set_memory_growth(devises[0], True)

train_dataset = DataLoader("D:\\PROJECTS\\TFRecord_Writing and Reading\\records", training=True).general_dataset()
validation_dataset = DataLoader("D:\\PROJECTS\\TFRecord_Writing and Reading\\records", training=False).general_dataset()
print(train_dataset)


def show(crop, title=None):
    plt.imshow(crop)
    if title:
        plt.title(title)
    plt.show()


for i, batch in enumerate(train_dataset):
    if i > 0:
        break
    show(batch[0][0].numpy())
    print(batch[1][0])



