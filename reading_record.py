import os
import glob
import random

import numpy
import tensorflow as tf
import imgaug.augmenters as iaa


def get_classes():
    classes = ["AnnualCrop",
               "Forest",
               "HerbaceousVegetation",
               "Highway",
               "Industrial",
               "Pasture",
               "PermanentCrop",
               "Residential",
               "River",
               "SeaLake"]
    return classes


def random_jitter(crop):
    crop = tf.image.resize(crop, [72, 72], method=tf.image.ResizeMethod.NEAREST_NEIGHBOR)
    crop = tf.image.random_crop(crop, size=[64, 64, 3])
    crop = tf.image.random_flip_up_down(crop)
    crop = tf.image.random_flip_left_right(crop)
    crop = tf.image.rot90(crop)
    noise = tf.random.normal(shape=tf.shape(crop), mean=0, stddev=0.02, dtype=tf.float32)
    crop = tf.add(noise, crop)

    return crop


def training_augmentations(crop):
    crop = crop.numpy()
    if random.random() < 0.5:
        sometimes = lambda aug: iaa.Sometimes(0.5, aug)
        seq = iaa.Sequential([sometimes(
            iaa.Affine(scale=(1.0, 1.1),
                       translate_percent={"x": (-0.1, 0.1), "y": (-0.1, 0.1)},
                       rotate=(-20, 20),
                       shear=(-8, 8),
                       mode='edge'))])
        crop = seq.augment_image(crop)
    return crop


def augmentation(crop, label):
    choice = tf.random.uniform([], minval=0, maxval=1, dtype=tf.float32)
    shape = crop.get_shape()
    crop = tf.py_function(training_augmentations, inp=[crop], Tout=tf.float32)
    crop.set_shape(shape)
    crop = tf.cond(choice < 0.5, lambda: crop, lambda: random_jitter(crop))
    return crop, label


class DataLoader(object):
    def __init__(self, data_path, training=True):
        self.data_path = data_path
        self.training = 'train' if training else 'validation'
        self.classes = get_classes()
        self.seed = 1
        if self.training == 'train':
            self.batch_size = 128
            self.buffer = 1000
        else:
            self.batch_size = 128
            self.buffer = 100

    def parse_record(self, record):
        features = {
            'image': tf.io.FixedLenFeature([], dtype=tf.string),
            'height': tf.io.FixedLenFeature([], dtype=tf.int64),
            'width': tf.io.FixedLenFeature([], dtype=tf.int64),
            'label': tf.io.FixedLenFeature([], dtype=tf.int64),
        }
        record = tf.io.parse_single_example(record, features)
        img = tf.io.decode_raw(record['image'], tf.float32)
        img = tf.reshape(img, [record['height'], record['width'], 3])
        label = tf.one_hot(record['label'], len(self.classes), dtype=tf.float32)
        return img, label

    def general_dataset(self):

        files = os.path.join(self.data_path, '{}_*.tf_record'.format(self.training))
        filenames = glob.glob(files)
        dataset = tf.data.Dataset.list_files(files, shuffle=True, seed=self.seed)
        dataset = dataset.interleave(lambda fn: tf.data.TFRecordDataset(fn), cycle_length=len(filenames),
                                     num_parallel_calls=min(len(filenames), tf.data.experimental.AUTOTUNE))
        dataset = dataset.map(self.parse_record, num_parallel_calls=tf.data.experimental.AUTOTUNE)
        if self.training == 'train':
            dataset = dataset.map(augmentation, num_parallel_calls=tf.data.experimental.AUTOTUNE)
            dataset = dataset.shuffle(self.buffer, seed=self.seed)
            dataset = dataset.repeat(1)
            dataset = dataset.prefetch(1)
        else:
            dataset = dataset.repeat(1)
        dataset = dataset.batch(self.batch_size)
        return dataset


if __name__ == '__main__':
    train_dataset = DataLoader("D:\\PROJECTS\\TFRecord_Writing and Reading\\records", training=True).general_dataset()
    validation_dataset = DataLoader("D:\\PROJECTS\\TFRecord_Writing and Reading\\records", training=False).general_dataset()
