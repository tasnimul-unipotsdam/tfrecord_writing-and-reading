import os
from pprint import pprint
import random
import numpy as np
import tensorflow as tf
from tensorflow.keras.models import load_model
from sklearn.metrics import classification_report, confusion_matrix
import json

os.environ['TF_CPP_MIN_LOG_LEVEL'] = '1'
devises = tf.config.experimental.list_physical_devices('GPU')
tf.config.experimental.set_memory_growth(devises[0], True)
random.seed(1001)

category = ["AnnualCrop",
            "Forest",
            "HerbaceousVegetation",
            "Highway",
            "Industrial",
            "Pasture",
            "PermanentCrop",
            "Residential",
            "River",
            "SeaLake"]

"""
Loading the Test Data
"""
X_test = np.load("X_test.npy")
y_test = np.load("y_test.npy")

"""
Pixel Normalization
"""
X_test = X_test / 255

model = load_model("model2.h5")

"""
Prediction of probability and classes of test data. 
"""
prediction_probability = model.predict(X_test)
prediction_class = prediction_probability.argmax(axis=-1)


def threshold_confusion_matrix(probabilities, labels):
    report = dict(results=[])
    pred_classes = probabilities.argmax(axis=-1).tolist()
    for t in [0.5, 0.6, 0.7, 0.85, 0.9, 0.95]:
        filtered_predictions = []
        filtered_labels = []
        for i, p in enumerate(probabilities):
            if p[pred_classes[i]] >= t:
                filtered_predictions.append(pred_classes[i])
                filtered_labels.append(int(labels[i]))

        conf_matrix = confusion_matrix(filtered_labels, filtered_predictions)

        meta = {
            'threshold_{}'.format(t): t,
            'confusion_matrix': conf_matrix.tolist(),
            'class_report': classification_report(filtered_labels, filtered_predictions,
                                                  target_names=category, output_dict=True)
        }
        report['results'].append(meta)
    return report


report_CM = threshold_confusion_matrix(prediction_probability, y_test)
dict_file = open("cm.json", "w")
json.dump(report_CM, dict_file, indent=4)
dict_file.close()
pprint(report_CM)
