import os
import random

import datetime


import tensorflow as tf
from tensorflow.keras.losses import categorical_crossentropy
from tensorflow.keras.optimizers import Adam


from reading_record import DataLoader
from model import model_vgg
random.seed(1001)

os.environ['TF_CPP_MIN_LOG_LEVEL'] = '1'
devises = tf.config.experimental.list_physical_devices('GPU')
tf.config.experimental.set_memory_growth(devises[0], True)

dict_file = open("weights.json", "r")
class_weight = dict_file.read()

train_dataset = DataLoader("D:\\PROJECTS\\TFRecord_Writing and Reading\\records", training=True).general_dataset()
validation_dataset = DataLoader("D:\\PROJECTS\\TFRecord_Writing and Reading\\records", training=False).general_dataset()

print(train_dataset)


def compile_model():
    model = model_vgg()
    model.summary()
    model.compile(optimizer=Adam(learning_rate=0.0001), loss=categorical_crossentropy, metrics=["accuracy"])
    return model


def train_model(model):
    log_dir = "logs\\fit\\" + datetime.datetime.now().strftime("%Y%m%d-%H%M%S")
    board = tf.keras.callbacks.TensorBoard(log_dir=log_dir, histogram_freq=1)

    history = model.fit(train_dataset, verbose=2, epochs=100, class_weight=class_weight,
                        validation_data=validation_dataset,  callbacks=[board])

    train_score = model.evaluate(train_dataset, verbose=0)
    print('train loss, train acc:', train_score)

    validation_score = model.evaluate(validation_dataset,  verbose=0)
    print('validation loss, validation acc:', validation_score)

    model.save("model_noise_.02.h5")


if __name__ == '__main__':
    compile_model = compile_model()
    train_model(compile_model)
    pass
