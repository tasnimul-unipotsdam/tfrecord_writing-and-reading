In this project, a pipeline for big dataset has created by using TFRecords. Although the dataset contains 27000 images which can be handled by tf.data.Dataset API, but TFRecords can handle big dataset.

Download link for the Dataset: https://github.com/phelber/eurosat

Folder Structure:   

                    train
                      Category01
                        image011.jpg
                        image012.jpg
                      category02
                        image021.jpg
                        image022.jpg


                     Validation
                      Category01
                        image018.jpg
                        image019.jpg
                      category02
                        image028.jpg
                        image029.jpg

Before running writing_record.py, create a "records" folder in the project directory.

References: The references for the dataset.
[1] Eurosat: A novel dataset and deep learning benchmark for land use and land cover classification. Patrick Helber,
Benjamin Bischke, Andreas Dengel, Damian Borth. IEEE Journal of Selected Topics in Applied Earth Observations and
Remote Sensing, 2019.
[2] Introducing EuroSAT: A Novel Dataset and Deep Learning Benchmark for Land Use and Land Cover Classification.
Patrick Helber, Benjamin Bischke, Andreas Dengel. 2018 IEEE International Geoscience and Remote Sensing Symposium, 2018.

Citation: For one of the image augmentation Part from imgaug. 

@misc{imgaug,
  author = {Jung, Alexander B.
            and Wada, Kentaro
            and Crall, Jon
            and Tanaka, Satoshi
            and Graving, Jake
            and Reinders, Christoph
            and Yadav, Sarthak
            and Banerjee, Joy
            and Vecsei, Gábor
            and Kraft, Adam
            and Rui, Zheng
            and Borovec, Jirka
            and Vallentin, Christian
            and Zhydenko, Semen
            and Pfeiffer, Kilian
            and Cook, Ben
            and Fernández, Ismael
            and De Rainville, François-Michel
            and Weng, Chi-Hung
            and Ayala-Acevedo, Abner
            and Meudec, Raphael
            and Laporte, Matias
            and others},
  title = {{imgaug}},
  howpublished = {\url{https://github.com/aleju/imgaug}},
  year = {2020},
  note = {Online; accessed 01-Feb-2020}
}

