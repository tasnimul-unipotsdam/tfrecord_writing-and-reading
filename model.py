import os
import random

import tensorflow as tf
from tensorflow.keras.regularizers import l2

random.seed(1001)

os.environ['TF_CPP_MIN_LOG_LEVEL'] = '1'
devises = tf.config.experimental.list_physical_devices('GPU')
tf.config.experimental.set_memory_growth(devises[0], True)


def model_vgg():
    inputs = tf.keras.Input(shape=(64, 64, 3))
    x = inputs
    x = tf.keras.layers.Conv2D(64, 3, padding="same", kernel_regularizer=l2(.0001))(x)
    x = tf.keras.layers.ReLU()(x)
    x = tf.keras.layers.BatchNormalization()(x)

    x = tf.keras.layers.Conv2D(64, 3, padding="same", kernel_regularizer=l2(.0001))(x)
    x = tf.keras.layers.ReLU()(x)
    x = tf.keras.layers.BatchNormalization()(x)
    x = tf.keras.layers.MaxPool2D()(x)

    x = tf.keras.layers.Conv2D(128, 3, padding="same", kernel_regularizer=l2(.0001))(x)
    x = tf.keras.layers.ReLU()(x)
    x = tf.keras.layers.BatchNormalization()(x)

    x = tf.keras.layers.Conv2D(128, 3, padding="same", kernel_regularizer=l2(.0001))(x)
    x = tf.keras.layers.ReLU()(x)
    x = tf.keras.layers.BatchNormalization()(x)
    x = tf.keras.layers.MaxPool2D()(x)

    x = tf.keras.layers.Conv2D(256, 3, padding="same", kernel_regularizer=l2(.0001))(x)
    x = tf.keras.layers.ReLU()(x)
    x = tf.keras.layers.BatchNormalization()(x)

    x = tf.keras.layers.Conv2D(256, 3, padding="same", kernel_regularizer=l2(.0001))(x)
    x = tf.keras.layers.ReLU()(x)
    x = tf.keras.layers.BatchNormalization()(x)
    x = tf.keras.layers.MaxPool2D()(x)

    x = tf.keras.layers.Conv2D(512, 3, padding="same", kernel_regularizer=l2(.0001))(x)
    x = tf.keras.layers.ReLU()(x)
    x = tf.keras.layers.BatchNormalization()(x)

    x = tf.keras.layers.Conv2D(512, 3, padding="same", kernel_regularizer=l2(.0001))(x)
    x = tf.keras.layers.ReLU()(x)
    x = tf.keras.layers.BatchNormalization()(x)
    x = tf.keras.layers.MaxPool2D()(x)

    x = tf.keras.layers.Flatten()(x)

    x = tf.keras.layers.Dense(64, kernel_regularizer=l2(.0001))(x)
    x = tf.keras.layers.ReLU()(x)
    x = tf.keras.layers.Dropout(.5)(x)

    x = tf.keras.layers.Dense(10, activation="softmax")(x)

    base_model = tf.keras.Model(inputs, x, name='base_model')
    return base_model


if __name__ == '__main__':
    model = model_vgg()
    model.summary()
